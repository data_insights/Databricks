from databricks_cli.sdk import ApiClient
from databricks_cli.jobs.api import JobsApi
from azure.identity import DefaultAzureCredential
import logging
import glob
import time
import json
import os
from typing import Dict, List
import argparse
from rest_api_auth import DbxResource, get_workspace_url, get_sp_auth_header


def init_logging() -> logging.Logger:
    logging.basicConfig()
    logger = logging.getLogger('dbx-deploy')
    logger.setLevel(logging.INFO)
    return logger


def get_jobs(workspace_url: str, api_token: str, additional_header={}) -> List:
    """Lists all jobs in a dbx workspace. A maximum of 25 jobs can be listed
    per API call.
    The while loop makes sure to get all existing jobs. A list of dictionaries
    containing job ids and job names is returned."""
    api_client = ApiClient(
        host=workspace_url,
        token=api_token,
        default_headers=additional_header,
        jobs_api_version="2.1"
    )
    job_api = JobsApi(api_client)

    jobs_list = []
    limit = 25
    offset = 0
    has_more = True

    while has_more:
        job_response = job_api.list_jobs(limit=limit, offset=offset)
        for job in job_response.get("jobs"):
            jobs_list.append({
             "job_id": job.get("job_id"),
             "name": job.get("settings").get("name")
            })

        offset += limit
        has_more = job_response.get("has_more")

    return jobs_list


def get_job_files(local_folder) -> List:
    file_list = []
    for path in glob.glob(f"{local_folder}/*.json"):
        clean_path = path.replace("\\", "/").replace("//", "/")
        file_list.append({
             "filepath": clean_path,
             "name":  os.path.basename(os.path.splitext(clean_path)[0])
        })
    return file_list


def create_job(workspace_url: str,
               api_token: str,
               file_name: str,
               logger: logging.Logger,
               additional_header={}):
    """
    Function to create a new job from a json file in the repo
    """
    api_client = ApiClient(
        host=workspace_url,
        token=api_token,
        default_headers=additional_header,
        jobs_api_version="2.1"
    )
    job_api = JobsApi(api_client)

    with open(file_name, "r") as file:
        data = file.read()

        job_settings = json.loads(data)

        response = job_api.create_job(job_settings)

    logger.info(f"{file_name} imported {response['job_id']}, {job_settings['name']}.")


def delete_job(workspace_url: str,
               api_token: str,
               job: dict,
               logger: logging.Logger,
               additional_header={}):
    """
    Function to delete existing jobs, jobs that start with _DEV_ are
    not deleted
    """
    api_client = ApiClient(
        host=workspace_url,
        token=api_token,
        default_headers=additional_header,
        jobs_api_version="2.1"
    )
    job_api = JobsApi(api_client)

    if not job.get("name").startswith("_DEV_"):
        time.sleep(1)
        response = job_api.delete_job(job.get("job_id"))
        logger.info(job["name"] + " was deleted.", response)


def update_job(workspace_url: str,
               api_token: str,
               job, logger: logging.Logger,
               additional_header={}):
    """
    Function to update existing jobs with new settings
    """
    api_client = ApiClient(
        host=workspace_url,
        token=api_token,
        default_headers=additional_header,
        jobs_api_version="2.1"
    )
    job_api = JobsApi(api_client)
    file_name = job["filename"]
    with open(file_name, "r") as file:
        data = file.read()
        job_settings = dict()
        job_settings["new_settings"] = json.loads(data)
        job_settings["job_id"] = job["job_id"]

        response = job_api.reset_job(job_settings)

        logger.info(f"{file_name} was updated on Databricks.", response)


def init_cli(logger: logging.Logger) -> Dict:
    cli_parser = argparse.ArgumentParser(description="invoke build steps")
    cli_params = {
        'subId': 'subscription id',
        'rsg': 'resource group name',
        'workspace': 'dbx workspace name',
        'localFolder': 'local folder with job jsons'
    }
    for arg, desc in cli_params.items():
        cli_parser.add_argument(f'--{arg}',
                                metavar=arg,
                                type=str,
                                help=desc,
                                required=True)

    cli_args = cli_parser.parse_args()
    cli_config = vars(cli_args)
    logger.info(f"CLI params [ '{cli_config}' ]")
    return cli_config


def deploy_job_step() -> None:
    """Pipeline step to deploy databricks jobs"""

    logger = init_logging()
    config = init_cli(logger)
    logger.info("START deploy")

    resource_config = {
        'subscription_id': config['subId'],
        'resource_group_name': config['rsg'],
        'workspace_name': config['workspace']
    }
    azr_cred = DefaultAzureCredential(exclude_shared_token_cache_credential=True)
    dbx_resource = DbxResource(**resource_config)

    workspace_url = get_workspace_url(dbx_resource, azr_cred, logger)
    sp_auth_header = get_sp_auth_header(dbx_resource, azr_cred, logger)

    dbx_token = azr_cred.get_token(*['2ff814a6-3304-4ab8-85cb-cd0e6f879c1d'])
    # get all jobs that are currently deployed on workspace
    job_list_online = get_jobs(workspace_url, dbx_token.token, sp_auth_header)
    # get list of fiels with jobs expected online
    job_file_list = get_job_files(config["localFolder"])

    job_names_online = [item["name"] for item in job_list_online]
    job_names_files = [item["name"] for item in job_file_list]
    # create list of name online and offline
    jobs_to_create_names = list(set(job_names_files) - set(job_names_online))
    jobs_to_delete_names = list(set(job_names_online) - set(job_names_files))

    # jobs offline but not online should be created new
    jobs_to_create = [item["filepath"] for item in job_file_list
                      if item["name"] in jobs_to_create_names]

    for job in jobs_to_create:
        create_job(workspace_url, dbx_token.token, job, logger, sp_auth_header)
    # jobs online but not offline should be deleted
    jobs_to_delete = [item for item in job_list_online
                      if item["name"] in jobs_to_delete_names]

    for job in jobs_to_delete:
        delete_job(workspace_url, dbx_token.token, job, logger, sp_auth_header)
    # jobs online and offline should be updated
    jobs_to_update_names = list(set(job_names_online) & set(job_names_files))
    jobs_to_update = []
    for name in jobs_to_update_names:
        data = dict()
        data["name"] = name
        data["job_id"] = [item["job_id"] for item in job_list_online if item["name"] in name][0]
        data["filename"] = [item["filepath"] for item in job_file_list if item["name"] in name][0]
        jobs_to_update.append(data)
    for job in jobs_to_update:
        update_job(workspace_url, dbx_token.token, job, logger, sp_auth_header)


if __name__ == '__main__':
    deploy_job_step()
