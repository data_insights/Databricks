from azure.identity import DefaultAzureCredential
import logging
import requests
from dataclasses import dataclass
from typing import Dict


@dataclass
class DbxResource:
    subscription_id: str
    resource_group_name: str
    workspace_name: str

    def __repr__(self):
        return f"workspace [ '{self.workspace_name}' ] IN group [ '{self.resource_group_name}' ]"


# region auth
def get_workspace_url(dbx_resource: DbxResource, azure_credential: DefaultAzureCredential, logger: logging.Logger) -> str:
    """Get workspace url from azure resource manager required to contact the dbx rest api"""
    base_uri = 'https://management.azure.com'
    azure_token = azure_credential.get_token(f'{base_uri}/.default')
    resource_group_uri = f'{base_uri}/subscriptions/{dbx_resource.subscription_id}/resourceGroups/{dbx_resource.resource_group_name}'
    provider_uri = f'{resource_group_uri}/providers/Microsoft.Databricks/workspaces/{dbx_resource.workspace_name}'
    uri = f'{provider_uri}?api-version=2018-04-01'

    logger.info(f"GET workspace url FOR resource {dbx_resource}")
    response = requests.get(uri, headers={'Authorization': f'Bearer {azure_token.token}'})
    parsed_response = response.json()
    workspace_url = parsed_response['properties']['workspaceUrl']
    workspace_url = 'https://' + workspace_url if workspace_url is not None else None

    logger.info(f"WORKSPACE url [ '{workspace_url}' ]")
    return workspace_url


def get_sp_auth_header(dbx_resource: DbxResource, azure_credential: DefaultAzureCredential, logger: logging.Logger) -> Dict:
    """Get service principal auth header required for service principal authentication to dbx"""
    base_uri = 'https://management.core.windows.net'
    azure_token = azure_credential.get_token(f'{base_uri}//.default')
    resource_group_uri = f'/subscriptions/{dbx_resource.subscription_id}/resourceGroups/{dbx_resource.resource_group_name}'
    resource_id = f'{resource_group_uri}/providers/Microsoft.Databricks/workspaces/{dbx_resource.workspace_name}'

    logger.info("GET header FOR service principal")
    sp_auth_header = {
        'X-Databricks-Azure-SP-Management-Token': azure_token.token,
        'X-Databricks-Azure-Workspace-Resource-Id': resource_id
    }
    return sp_auth_header
